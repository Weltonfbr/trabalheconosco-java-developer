# Desenvolvedor Android - Trabalhe conosco

# Objetivo

Desenvolver um app nativo para a plataforma Android que permite ao usuário pesquisar e ver um perfil do Github(nome e foto) e listar seus repositórios.

Por favor utilize as seguintes URL para fazer requisições da API:

[https://api.github.com/users/nomedousuario](https://api.github.com/users/nomedousuario) - Retorna os detalhes do usuário

[https://api.github.com/users/nomedousuario/repos](https://api.github.com/users/nomedousuario/repos) - Retorna os repositórios do usuário

# Como fazer?
 - Fazer um fork no master , executar todas tarefas abaixo
 - Comitar com seu nome, telefone e email.
 

# Funcionalidades

- Tela inicial - Composta por um EditText(onde o nome do usuário do GitHub é digitado) e um botão (onde é disparada a requisição para a API do GitHub, após o retorno da requisição é levado para a tela de perfil do usuário)
- Tela de detalhes do perfil - Composta por um ImageView com o avatar do usuário, um TextView com o nome do usuário e uma lista de repositórios.
- Tratamento de erros, caso não encontre o usuário deverá retornar a seguinte mensagem na tela (Usuário não encontrado. Por favor digite outro nome de usuário), qualquer outro erro deverá retornar: Ops! Aconteceu um erro. Verifique sua conexão com a internet e tente novamente.

# Requisistos
  - Aplicativo desenvolvido utilizando ferramentas e SDKs nativos (Android Studio + Java ou Kotlin)
  - Seguir sempre que possível as imagens de layouts abaixo.
  - Você pode utilizar qualquer biblioteca para desenvolver o app (Retrofit, RxAndroid, etc...)
  - 
  


![Tela inicial e de Perfil](https://i.imgur.com/yJ4hrwN.jpg)